package modulo_3_4;

import java.util.Scanner;

public class ejercicio8 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int pj1, pj2;
		System.out.print("Pj1: Elija Piedra (0), Papel (1), o Tijeras (2) --> ");
		pj1 = scan.nextInt();
		System.out.print("Pj2: Elija Piedra (0), Papel (1), o Tijeras (2) --> ");
		pj2 = scan.nextInt();
		
		if(pj1==0) {
			if(pj2==1) {
				System.out.println("El Pj 2 gana");
			}
			else if(pj2==2) {
				System.out.println("El Pj 1 gana");
			}
			else {
				System.out.println("Ambos empatan");
			}
		}
		else if(pj1==1) {
			if(pj2==0) {
				System.out.println("El Pj 1 gana");
			}
			else if(pj2==2) {
				System.out.println("El Pj 2 gana");
			}
			else {
				System.out.println("Ambos empatan");
			}
		}
		else if(pj1==2) {
			if(pj2==0) {
				System.out.println("El Pj 2 gana");
			}
			else if(pj2==1) {
				System.out.println("El Pj 1 gana");
			}
			else {
				System.out.println("Ambos empatan");
			}
		}
		scan.close();

	}

}
