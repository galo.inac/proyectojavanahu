package modulo_3_4;

import java.util.Scanner;

public class ejercicio11 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Coloque una letra --> ");
		char letra = scan.next().charAt(0);
		if(letra=='a'||letra=='e'||letra=='i'||letra=='o'||letra=='u') {
			System.out.println("Tu letra es una vocal");
		}
		else {System.out.println("Tu letra es una consonante");}
		scan.close();
	}
}
