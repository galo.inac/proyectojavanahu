package modulo_3_4;

import java.util.Scanner;

public class ejercicio13 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Coloque un mes --> ");
		String mes = scan.nextLine();
		switch(mes){
		case "febrero":
			System.out.println("Es un mes de 28 d�as");
			break;
		case "abril": case "junio": case "septiembre": case "noviembre":
			System.out.println("Es un mes de 30 d�as");
			break;
		case "enero": case "marzo": case "mayo": case "julio": case "agosto": case "octubre": case "diciembre":
			System.out.println("Es un mes de 31 d�as");
			break;
		default:
			System.out.println("Error");
		}
		scan.close();
	}

}
