package modulo2;

public class ejercicio2 {

	public static void main(String[] args) {
		
		byte bmin = (byte)Math.pow(2, 7);
		byte bmax = (byte) ((byte)Math.pow(2, 8-1)-1);
		short smin = (short)Math.pow(2, 15);
		short smax = (short) ((short)Math.pow(2, 15)-1);
		int imax = (int)Math.pow(2, 31)-1;
		int imin = (int)Math.pow(2, 31);
		long lmin = (long)Math.pow(2, 63)-1;
		long lmax = (long)Math.pow(2, 63);
		System.out.println("Tipo de Dato\tM�nimo\tM�ximo");
		System.out.println("\nbyte\t"+bmin+"\t\t\t"+bmax);
		System.out.println("\nshort\t"+smin+"\t\t\t"+smax);
		System.out.println("\nint\t"+imin+"\t\t"+imax); 
		System.out.println("\nlong\t"+lmin+"t"+lmax);

	}

}
